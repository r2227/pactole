Structure de "src"
===========================

> Liste des dossiers et fichiers du dossier 'src'
### Contenu
    . src
    ├── components     # Bibliothèque des Components VueJS
    ├── engine         # Moteur de Jeu
    ├── models         # Modèles 3D
    ├── App.vue
    ├── main.js
    └── README.md
   

### Components

Ce dossier contient tout les éléments VueJS utilisés au sein du jeu.

### Engine

L'ensemble des éléments requis au fonctionnement du jeu (Moteur réseau/graphique et Contrôleur)

### Models

Tout les éléments 3D sont stockés dans ce dossier.
