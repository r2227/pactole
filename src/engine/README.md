Structure de "engine"
===========================

> Liste des dossiers et fichiers du dossier 'engine'
### Contenu
    .
    ├── data                        # Dossier data :
    │   ├── party                           # Dossier où les données d'une partie sont enregistrées
    │   │   └── 1.json                         # 1/45 (on peut en avoir plusieurs)
    │   └── presets                         # Dossier comportant les données communes a toutes les parties
    │       └── chunks.json                     #
    ├── entities                    # Dossier comportant les version logiques des composants de Pactole
    │   ├── Castle.js                      #
    │   ├── Map.js                         # 
    │   ├── Pawn.js                        #
    │   ├── Player.js                      # 
    │   ├── Rules.js                       #
    │   └── Well.js                        #
    ├── net                         # Composant responsable des interactions réseaux
    │   ├── ConnectionHandler.js            # 
    │   ├── CryptideIO.js                   # 
    │   └── Players.js                      # 
    ├── ui                          # Composant responsable de l'environnement 3D
    │   ├── components                      # 
    │   │   └── Chunk.js                        # Ensemble de 18 tuiles
    │   ├── controllers                     # 
    │   │   ├── CameraController.js             # Contrôleur de la camera
    │   │   └── RaycasterController.js          # Contrôleur du systeme de selection de tuiles
    │   ├── entities                    # Dossier comportant les objets 3D manipulés par THREEJS
    │   │   ├── Castle.js                   # 
    │   │   ├── Entity.js                   # 
    │   │   ├── Pawn.js                     # 
    │   │   ├── Tiles.js                    # 
    │   │   └── Well.js                     # 
    │   └── CryptideUI.js              # Contrôleur 3D
    └── game.js                    # Contrôleur logique du jeu