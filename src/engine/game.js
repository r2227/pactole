"use strict";
import EventEmitter from "eventemitter3";
import CryptideUI from './ui/CryptideUI';
import CryptideIO from './net/CryptideIO';

import Map from './entities/Map';
import Rules from './entities/Rules';
import Player from "./entities/Player";

/*
    Game logic class
 */
export default class Cryptide extends EventEmitter {
    // Client variables
    #debug = false;
    #canvas = null;
    #io = null;

    // Game variables
    #threeWorld = null;

    // Players
    #players = [];
    #playerMaster = null;

    // Party variables
    #partyStatus  = 'none';

    #partyId = null;
    #partyDifficulty = null;
    #partyOrder = [];
    #partyMap = [];
    #partyRules = [];
    #partyRulesSetting = 0;

    constructor({
        debug = false,
        canvas = null
    }) {
        super();
        this.#io = new CryptideIO(debug);
        this.#debug = debug;
        this.#canvas = canvas;
        this.#threeWorld = new CryptideUI(canvas, debug);
        this.#partyMap = new Map(this.#threeWorld);
        this.ioListener();
    }

    ioListener(){
        this.#io.on('data', (d) => {
            if (this.#debug)
                console.log('[Cryptide/Logic] Received data from server:', d);
            switch (d.type){
                case 'startGame':
                    this.#partyStatus = 'game';
                    // Set the order
                    this.#partyOrder = d.data.order;
                    break;
                case 'newPlayer':
                    let newColor = Player.getRandomColorName();
                    while (this.#players.find(p => p.color === newColor)){
                        newColor = Player.getRandomColorName();
                    }
                    let nPlayer = new Player({id: d.data.peerId, username: d.data.username, color: newColor});
                    this.#players.push(nPlayer);
                    if (this.#playerMaster === this.#io.me.id){
                        // Synchronize with all other players the new color
                        this.#io.sendDataToAll(this.#io.buildPayload('updatePlayer', this.#players.map(p => p.getData())));
                    }
                    d.data = nPlayer;
                    break;
                case 'playerQuit':
                    let player = this.#players.find(p => p.id === d.data.peerId);
                    this.#players = this.#players.filter(p => p.id !== d.data.peerId);
                    d.data = player;
                    break;
                case 'updatePlayer':
                    for(let p of d.data){
                        let player = this.#players.find(d => d.id === p.id);
                        if (player){
                            player.update(p);
                        }
                    }
                    break;
                case 'message':
                    break;
                case 'selectTile':
                    this.emit(d.origin + ':selectTile', d.data);
                    break;
                case 'roundCryptid':
                    this.emit(d.origin + ':roundCryptid', d.data);
                    break;
                case 'roundPlayer':
                    this.emit(d.origin + ':roundPlayer', d.data);
                    break;
            }
            // Sending to the UI
            this.emit('data', d);
        })
    }

    /**
     * Create a party
     */
    async createParty({username}){
        this.#players = [];
        let p = await this.#io.createParty({peerMaster: this.#io.peer, username: username});
        this.#players.push(new Player({id: p.myId, username: username, color: Player.getRandomColorName()}));

        this.#playerMaster = this.#players[0].id;
        this.#partyStatus = 'lobby';
        this.#partyId = p.partyId;

        if (this.#debug) {
            console.log(`Party created with id: ${this.#partyId}`);
        }
        return {
            myId: p.myId,
            partyId: p.partyId,
            players: this.#players
        }
    }
    /**
     * Start the game
     */
    async startParty(){
        if (this.#partyStatus !== 'lobby'){
            throw new Error('Party is not in lobby');
        }

        this.#partyStatus = 'game';
        // Create passage order
        this.#partyOrder = this.#players.map(p => p.id);
        // Shuffle
        this.#partyOrder = this.#partyOrder.sort(() => Math.random() - 0.5);
        let p = await this.#io.startParty({order: this.#partyOrder});
        return true;
        /*
        this.#partyDifficulty = p.difficulty;

        this.loadParty(GameConfig);
         */
    }

    /**
     * Join a party
     * @param {string} partyId
     * @param {string} username
     * @returns {Promise<void>}
     */
    joinParty({partyId, username}){
        this.#players = [];
        return new Promise((resolve, reject) => {
            this.#io.joinParty({peerId: partyId, username: username}).then(d => {
                // Add players to our list
                for (let i = 0; i < d.players.length; i++) {
                    this.#players.push(new Player({id: d.players[i].peerId, username: d.players[i].username, color: Player.getRandomColorName()}));
                }
                this.#partyStatus = 'lobby';
                this.#partyId = d.partyId;
                this.#playerMaster = this.#players[0].id;
                // Send the party info to the UI
                resolve({
                    myId: d.myId,
                    partyId: d.partyId,
                    players: this.#players
                });
            }).catch(e => {
                // Error joining the party :/
                reject(e);
            });
        });
    }

    /**
     * Charge une partie selon les configurations JSON
     * @param party
     * @returns {Promise<void>}
     */
    async loadParty(party){
        // WorldMap
        this.#partyMap.loadMap(party);
        this.#threeWorld.loadScene(party);

        // Rules
        for (let x in party.rules){
            let rules = [];
            for (let y in party.rules[x]){
                rules.push(new Rules(party.rules[x][y]));
            }
            this.#partyRules.push(rules);
        }
        this.#partyRulesSetting = this.#players.length;
        this.emit('data', {type : 'rule', rule : this.#partyRules[this.#partyRulesSetting - 3][this.#partyOrder.indexOf(this.#io.me.id)].toString()});
        this.gameLoop();
    }

    async gameLoop(){

        let partyRules = this.#partyRules[this.#partyRulesSetting - 3];
        let i = 0;
        let currentTurn = 0;
        let winner = null;

        //Classic Turns

        while(winner === null){
            let possibleMoves;
            let type;
            let position = {x: 0, y: 0};
            let requireInteraction = false;
            if(currentTurn < 2*this.#partyRulesSetting) {
                possibleMoves = partyRules[i].getUnavailableMoves(this.#partyMap);
                type = 'no';
            }
            else {
                possibleMoves = partyRules[i].getAvailableMoves(this.#partyMap);
                type = 'yes';
                requireInteraction = true;
            }
            this.emit('dataTurn', {type : 'newTurn',currentPlayer : this.#partyOrder[i], self: this.#partyOrder[i] === this.#io.me.id});
            // Is it my turn?
            if (this.#partyOrder[i] === this.#io.me.id){
                for (let move of possibleMoves){
                    let d = this.#threeWorld.getTile(move);
                    this.#threeWorld.raycasterController.addSelectable(d);
                }
                let tile, choice;
                let positions;
                if (this.#debug)
                    console.log('My turn!', this.#io.me.id);

                if (requireInteraction){
                    // Wait for the user to do a choice on the UI
                    choice = await this.playerAction(this.#io.me.id, 'selectVue')
                    switch (choice.type){
                        case 'tiles':
                            break;
                        case 'player':
                            this.#threeWorld.raycasterController.clearSelectable();
                            let tileChoice = this.#partyMap.clickeableTiles;

                            for (let move of tileChoice){
                                let d = this.#threeWorld.getTile(move);
                                this.#threeWorld.raycasterController.addSelectable(d);
                            }
                            break;
                    }
                }

                // Wait for the user to select a tile (ThreeJS)
                tile = await this.#threeWorld.selectTile();
                this.#threeWorld.raycasterController.clearSelectable();
                positions = tile.getGlobalCoordinates();

                this.selectTile({
                    x: positions[0],
                    y: positions[1],
                    context: choice
                });
                if (currentTurn < 2*this.#partyRulesSetting){
                    let myPlayer = this.#players.find(p => p.id === this.#io.me.id);
                    tile.addPawn( myPlayer.color,type);
                    this.#partyMap.addPawn(positions[0], positions[1], myPlayer.color, type, myPlayer.id);
                }else{
                    await this.round(true, this.#io.me.id, {
                        x: positions[0],
                        y: positions[1],
                        context: choice
                    });
                }
                position = {x: positions[0], y: positions[1]};
            }else{
                if (this.#debug)
                    console.log('Waiting on someone else', this.#partyOrder[i]);
                let p = await this.playerAction(this.#partyOrder[i], 'selectTile');
                if (currentTurn < 2*this.#partyRulesSetting){
                    let tile = this.#threeWorld.getTile({x: p.x, y: p.y});
                    let poser = this.#players.find(p => p.id === this.#partyOrder[i]);
                    tile.addPawn( poser.color,type);
                    this.#partyMap.addPawn(p.x, p.y, poser.color, type, poser.id);
                }else{
                    await this.round(false, this.#partyOrder[i],{
                        x: p.x,
                        y: p.y,
                        context: p.context
                    });
                }
                position = {x: p.x, y: p.y};
            }

            // Check for the winner by checking if the Pawn count is equal to the number of players and there's no "no" pawns
            let tilePawns = this.#partyMap.getTilePawns(position.x, position.y);
            if (tilePawns.length >= this.#partyOrder.length && !tilePawns.find(p => p.type === 'no')){
                winner = this.#partyOrder[i];
            }

            this.#threeWorld.raycasterController.clearSelectable();
            i++;
            if(i === this.#partyRulesSetting){
                i = 0;
            }
            currentTurn++;
        }

        // We have a winner
        this.emit('dataTurn', {type : 'winner', name: this.#players.find(p => p.id === winner).username});
    }

    async round(self, id, {x, y, context}) {
        let player;
        let tile = this.#threeWorld.getTile({x: x, y: y});
        tile.select();
        tile.enableHighlight();
        let positions;
        switch (context.type){
            case 'player':
                let targetId  = context.id;
                let rule = this.#partyRules[this.#partyRulesSetting - 3][this.#partyOrder.indexOf(targetId)];
                let type = (rule.isValidMove(this.#partyMap, {x: x, y: y})) ? 'yes' : 'no';

                if (targetId === this.#io.me.id){
                    // I have to reply
                    this.emit('dataTurn', {type : 'roundPlayer', x: x, y: y, answer: type});
                    await this.playerAction(targetId, 'roundPlayer');
                }else{
                    // Waiting
                    await this.playerAction(targetId, 'roundPlayer');
                }

                // Adding the response
                player = this.#players.find(p => p.id === targetId);
                tile.addPawn( player.color, type);
                this.#partyMap.addPawn(x, y, player.color, type, targetId);
                tile.unselect();
                tile.disableHighlight();

                if (type === 'no'){
                    // console.log('the other player must put a yes somewhere else');
                    player = this.#players.find(p => p.id === id);
                    if (id === this.#io.me.id){
                        // I have to reply
                        this.#threeWorld.raycasterController.clearSelectable();
                        let possibleMoves = this.#partyRules[this.#partyRulesSetting - 3][this.#partyOrder.indexOf(id)].getUnavailableMoves(this.#partyMap);
                        for (let move of possibleMoves){
                            let d = this.#threeWorld.getTile(move);
                            this.#threeWorld.raycasterController.addSelectable(d);
                        }
                        this.emit('dataTurn', {type : 'alert',message: "Il faut poser un non sur une case !"});
                        tile = await this.#threeWorld.selectTile();
                        positions = tile.getGlobalCoordinates();

                        this.getAction('roundPlayer', {x: positions[0], y: positions[1]});
                    }else{
                        // Waiting for the chosen tile
                        let choice = await this.playerAction(id, 'roundPlayer');
                        positions = [choice.x, choice.y];

                    }
                    tile = this.#threeWorld.getTile({x: positions[0], y: positions[1]});
                    tile.addPawn( player.color, 'no');
                    this.#partyMap.addPawn(x, y, player.color, 'no', targetId);
                }
                break;

            case 'tiles':
                // The Pawn
                player = this.#players.find(p => p.id === id);
                tile.addPawn( player.color, 'yes');
                this.#partyMap.addPawn(x, y, player.color, 'yes', id);

                // Rotate on other players from the Index
                let i = this.#partyOrder.indexOf(id);
                for (let j = 0; j < this.#partyRulesSetting; j++){
                    let player = this.#players.find(p => p.id === this.#partyOrder[(i+j)%this.#partyRulesSetting]);
                    let rule = this.#partyRules[this.#partyRulesSetting - 3][(i+j)%this.#partyRulesSetting];
                    let type = (rule.isValidMove(this.#partyMap, {x: x, y: y})) ? 'yes' : 'no';
                    if (player.id === this.#io.me.id && !self){
                        this.emit('dataTurn', {type : 'roundCryptid', x: x, y: y, answer: type});
                        await this.playerAction(player.id, 'roundCryptid');
                    }else if (id !== player.id){
                        await this.playerAction(player.id, 'roundCryptid');
                    }else{
                        continue;
                    }
                    let tile = this.#threeWorld.getTile({x: x, y: y});
                    tile.addPawn( player.color, type);
                    this.#partyMap.addPawn(x, y, player.color, type, player.id);
                    tile.unselect();
                    tile.disableHighlight();
                    if (type === 'no'){
                        break;
                    }
                }
                break;
            default:
                return new Error('Unknown context type');
        }
    }
    // Interactions

    sendMessage(message){
        this.#io.sendMessage(message);
    }
    selectTile({x, y, context}){
        this.#io.selectTile({x, y, context});
    }
    sendInteraction(type, data){
        this.#io.sendInteraction(type, data);
    }

    async playerAction(id, action){
        return new Promise(function(resolve, reject){
            switch (action){
                case 'selectTile':
                case 'selectVue':
                case 'roundCryptid':
                case 'roundPlayer':
                    break;
                default:
                    reject('Unknown action');
            }
           this.once(id + ':' + action, (data) => {
               resolve(data);
           });
        }.bind(this));
    }
    getAction(action, obj){
        this.emit(this.#io.me.id + ':' + action, obj);
        switch (action){
            case 'roundCryptid':
            case 'roundPlayer':
                this.sendInteraction(action, obj);
                break;
        }
    }
    // Canvas resize
    resizeCanvas() {
        this.#canvas.style.width = '100%';
        this.#canvas.style.height= '100%';
        this.#canvas.width  = window.innerWidth;
        this.#canvas.height = window.innerHeight;
        this.#threeWorld.onWindowResize();
    }
}