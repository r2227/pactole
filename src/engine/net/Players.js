"use strict";

export default class Players {
    #username = null;
    #peerId = null;
    #peerTunnel = null;

    constructor({username, peerId, peerTunnel}) {
        this.#username = username;
        this.#peerId = peerId;
        this.#peerTunnel = peerTunnel;
    }

    get username() {
        return this.#username;
    }

    get peerId() {
        return this.#peerId;
    }

    get peerTunnel() {
        return this.#peerTunnel;
    }
}