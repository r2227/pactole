"use strict";

export default class ConnectionHandler {
    _currentType = 'client';
    _remotetype = 'client';
    _ctx = null;
    _dataConnection = null;

    /**
     *
     * @param {CryptideIO} ctx
     * @param {string} currentType
     * @param {string} remoteType
     * @param dataConnection
     */
    constructor(ctx, currentType, remoteType, dataConnection) {
        this._ctx = ctx;
        this._currentType = currentType;
        this._remoteType = remoteType;
        this._dataConnection = dataConnection;
        this.#init();
    }

    set remoteType(type) {
        if (['client', 'master', 'player'].indexOf(type) === -1) {
            throw new Error('Invalid type');
        }
        this._remoteType = type;
    }

    get remoteType() {
        return this._remoteType;
    }

    set currentType(type) {
        if (['client', 'master', 'player'].indexOf(type) === -1) {
            throw new Error('Invalid type');
        }
        this._currentType = type;
    }

    get currentType() {
        return this._currentType;
    }

    get dataConnection() {
        return this._dataConnection;
    }

    #init() {
        this._dataConnection.on('data', (payload) => {
            if (this._currentType === 'master' && this._remoteType === 'client') {
                // Im a Master talking to a non player client
                switch (payload.type) {
                    case 'newPlayer':
                        if (this._ctx.gameStatus === 'lobby' && this._ctx.players.length < 5) { // Lobby Player count check
                            // Sending game state to the new player
                            let player = {
                                username: payload.data.username,
                                peerId: this._dataConnection.peer,
                                peerTunnel: this
                            };
                            // Notify all players
                            let newPlayerPayload = this._ctx.buildPayload('newPlayer', {
                                username: player.username,
                                peerId: player.peerId
                            });
                            this._ctx.sendDataToAll(newPlayerPayload);
                            // Add the player to our list
                            this._remoteType = 'player';
                            this._ctx.addPlayer(player);
                            this._dataConnection.send({type: 'newPlayer', status: 'ok', ctx: this._ctx.getGameState()});
                            // Myself
                            this._ctx.emit('data', newPlayerPayload)
                        } else if (this._ctx.gameStatus === 'game') {
                            this._dataConnection.send({type: 'newPlayer', status: 'running'});
                        } else if (this._ctx.players.length >= 5) {
                            this._dataConnection.send({type: 'newPlayer', status: 'full'});
                        }
                        break;
                    default:
                        console.log('Unknown directive, closing connection');
                        this._dataConnection.close();
                }
                return;
            }
            if (this._remoteType === 'master') {
                // Im a client talking to a master
                switch (payload.type) {
                    case 'newPlayer':
                        this._ctx.addPlayer(payload.data);
                        break;
                    case 'playerQuit':
                        this._ctx.deletePlayer(payload.data.peerId);
                        break;
                    default:
                        break;
                }
            }
            // Send to myself
            this._ctx.emit('data', payload);
            // If im a master, send to all players except the sender
            if (this._currentType === 'master') {
                this._ctx.sendDataToAll(payload, [this._dataConnection.peer]);
            }
        });
        this._dataConnection.on('open', () => {

        });
        // WebRTC Chromium bug #1676
        this._dataConnection.peerConnection.oniceconnectionstatechange = (event) => {
            if (event.target.iceConnectionState === 'disconnected') {
                connectionClosed.bind(this)();
            }
        };
        this._dataConnection.on('close', () => {
            connectionClosed.bind(this)();
        });

        function connectionClosed() {
            if (this._remoteType === 'player') {
                this._ctx.deletePlayer(this._dataConnection.peer);
                if (this._currentType === 'master') {
                    let payload = this._ctx.buildPayload('playerQuit', {peerId: this._dataConnection.peer});
                    // Send to everyone else
                    this._ctx.sendDataToAll(payload);
                    // Myself
                    this._ctx.emit('data', payload)
                }
            }
            // Someone disconnected from me
        }

        this._dataConnection.on('error', (err) => {
            console.log(err);
        });
    }
}