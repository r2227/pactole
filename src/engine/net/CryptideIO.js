"use strict";
import EventEmitter from "eventemitter3";
import Peer from "peerjs";

import ConnectionHandler from "./ConnectionHandler.js";
import Players from "./Players.js";

export default class CryptideIO extends EventEmitter{
    #debug = false;
    // Peer variables
    #netType = 'client';

    #peer = null; // Own peer object
    #peerMaster = null; // Master peer object

    #conns = {}; // client[]

    #gameStatus = 'none'; // none, lobby, game
    #playersCon = []; // master|player[]
    /**
     * Initialize the CryptideIO class
     */
    constructor(debug) {
        super();
        this.#debug = debug;
        this.#peer = new Peer('PACT-' + CryptideIO.randomStr(20), {
            host: 'broker.roots.games',
            port: 443,
            secure: true,
            debug: 2
        });
        this.#peer.on('connection', (c) => {
            switch(this.#netType){
                case 'master':
                    this.#conns[c.peer] = new ConnectionHandler(this, this.#netType,'client',c);
                    break;
                case 'client':
                case 'player':
                    //   I shouldn't be receiving connection
                    if (this.#debug)
                        console.log('who are you?');
                    c.close();
                    break;
            }
        });
        this.#peer.on('disconnected', () => {
            // Unexpected disconnection from Broker
            if (this.#debug)
                console.log('Connection lost to Broker. (Internet crash?)');
            this.#peer.reconnect();
        });
        this.#peer.on('close', () => {
            if (this.#debug)
                console.log('All Connection destroyed');
        });
        this.#peer.on('error', (err) => {
            console.log(err);
        });
    }

    /**
     * Set the netType
     * @param {string} type
     */
    setType(type){
        if (!['master','player'].includes(type)){
            throw new Error('Invalid netType');
        }
        // Change current type in every connection
        for (let i in this.#conns){
            this.#conns[i].currentType = type;
        }
        this.#netType = type;
    }

    /**
     * Get the peer ID
     * @returns {{id}}
     */
    get me() {
        return {id: this.#peer.id};
    }
    /**
     * Get the peer Obj
     * @returns {null}
     */
    get peer() {
        return this.#peer;
    }

    /**
     * Get the game status
     * @returns {string}
     */
    get gameStatus() {
        return this.#gameStatus;
    }

    /**
     * Get the players
     * @returns {*[]}
     */
    get players() {
        return this.#playersCon;
    }

    /**
     * Get the game environment
     * @returns {{peerMasterId: string, gameStatus: string, players: {peerId: *, username: *}[]}}
     */
    getGameState(){
        let mastId = '';
        if (this.#netType === 'master'){
            mastId = this.#peerMaster.id;
        }else{
            mastId = this.#peerMaster.peer;
        }
        return {
            peerMasterId: mastId,
            gameStatus: this.#gameStatus,
            players: this.#playersCon.map(function (p) {
                return {
                    peerId: p.peerId,
                    username: p.username
                }
            })
        }
    }

    /**
     * Set the game environment
     * @param data
     */
    setGameState(data) {
        this.#gameStatus = data.gameStatus;
        for (let i = 0; i < data.players.length; i++) {
            if(data.players[i].peerId === this.#peerMaster.peer && this.#netType !== 'master')
                continue;
            this.addPlayer({
                peerId: data.players[i].peerId,
                username: data.players[i].username
            });
        }
    }

    async joinMaster(peerId){

    }

    async startParty({settings, order}){
        if (this.#netType !== 'master'){
            throw new Error('Not a master');
        }
        if (this.#gameStatus !== 'lobby'){
            throw new Error('Cannot start party');
        }
        this.#gameStatus = 'game';
        let payload = this.buildPayload('startGame',{settings, order}); // TODO: Add game settings & map
        this.sendDataToAll(payload);
    }
    /**
     * Create Party
     * @param peerMaster
     * @param username
     */
    async createParty({peerMaster, username}){
        // Setting up the variables
        this.setType('master');
        this.#gameStatus = 'lobby';
        this.#peerMaster = peerMaster;
        this.addPlayer({peerId: this.#peer.id, username: username});
        return {myId: this.me.id, partyId: this.#peerMaster.id};
    }

    /**
     * Join a Party
     * @param peerId
     * @param username
     */
    joinParty({peerId, username}) {
        this.setType('player');
        // Getting the real Master PeerID (Proxied room)

        return new Promise((resolve, reject) => {
            if (this.#debug)
                console.log('Joining Party' + peerId + ' ' + username);
            this.#peerMaster = this.#peer.connect(peerId);
            this.#peerMaster.on('open', () => {
                this.#peerMaster.send(this.buildPayload( 'newPlayer', {'username': username}));
            });
            this.#peerMaster.once('data', (data) => {
                if (data.type && data.type === 'newPlayer'){
                    switch (data.status){
                        case 'ok':
                            // Update state, forward to ConnectionHandler & Game
                            // ConnectionHandler
                            this.#conns[peerId] = new ConnectionHandler(this, this.#netType,'master', this.#peerMaster);
                            for (let i in data.ctx.players){
                                if (data.ctx.players[i].peerId === this.#peerMaster.peer){
                                    this.addPlayer({peerId: this.#peerMaster.peer, username: data.ctx.players[i].username, peerTunnel: this.#conns[peerId]});
                                }
                            }
                            // State
                            this.setGameState(data.ctx);
                            resolve({myId: this.me.id,partyId: this.#peerMaster.peer, players: this.#playersCon});
                            break;
                        case 'full':
                        case 'running':
                            reject(data.status);
                            break;
                    }
                }
            });

            // WebRTC Chromium bug #1676
            this.#peerMaster.peerConnection.oniceconnectionstatechange = (event) => {
                if (event.target.iceConnectionState === 'disconnected'){
                    if (this.#debug)
                        console.log('Connection to master lost by ICE');
                }
            };
            this.#peerMaster.on('close', () => {
                if (this.#debug)
                    console.log('Connection to master lost');
            });
            this.#peerMaster.on('error', (err) => {
                console.log(err);
            });
            this.#peer.once('error', () => {
                reject('could_not_connect');
            });
        });
    }

    /**
     * Add a player to the party
     * @param peerId
     * @param username
     * @param peerTunnel
     */
    addPlayer({peerId, username, peerTunnel}){
        this.#playersCon.push(new Players({peerId: peerId, username: username, peerTunnel: peerTunnel}));
    }

    /**
     * Remove a player from the party based on their peerId
     * @param peerId
     */
    deletePlayer(peerId){
        for (let i = 0; i < this.#playersCon.length; i++){
            if (this.#playersCon[i].peerId === peerId){
                this.#playersCon.splice(i, 1);
            }
        }
    }

    /**
     * Get the player based on their peerId
     * @param peerId
     * @returns {*}
     */
    getPlayer(peerId){
        for (let i = 0; i < this.#playersCon.length; i++){
            if (this.#playersCon[i].peerId === peerId){
                return this.#playersCon[i];
            }
        }
    }

    /**
     * Send a payload to all players
     * @param payload
     * @param blacklist
     */
    sendDataToAll(payload, blacklist = []){
        for (let i = 0; i < this.#playersCon.length; i++){
            if (this.me.id === this.#playersCon[i].peerId || blacklist.includes(this.#playersCon[i].peerId)){
            //    this.emit('data', payload);
                continue;
            }
            if (this.#playersCon[i].peerTunnel){
                this.#playersCon[i].peerTunnel.dataConnection.send(payload);
            }
        }
    }

    /**
     * Build a payload to be sent.
     * @param type
     * @param data
     */
    buildPayload(type, data){
        return {
            origin: this.me.id,
            type: type,
            data: data
        }
    }

    /*
        Interaction with the game
     */

    /**
     * Send a message to the game
     * @param {string} message
     */
    sendMessage(message){
        this.sendDataToAll(this.buildPayload('message', {message: message}));
    }

    /**
     * Select a tile
     * @param {int} x
     * @param {int} y
     * @param {Array} context
     */
    selectTile({x, y, context}){
        this.sendDataToAll(this.buildPayload('selectTile', {x: x, y: y, context: context}));
    }

    /**
     * Send interactions
     * @param {string} type
     * @param data
     */
    sendInteraction(type, data){
        this.sendDataToAll(this.buildPayload(type, data));
    }

    /**
     * Generate a random string of length n
     * @param {int} length
     * @returns {string}
     */
    static randomStr(length) {
        let result           = '';
        let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }
}
