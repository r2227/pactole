'use strict';

import * as THREE from "three";
import {TWEEN} from "three/examples/jsm/libs/tween.module.min";
import { GUI } from 'dat.gui'

import CameraController from "./controllers/CameraController";
import RaycasterController from "./controllers/RaycasterController";

import Chunk from "./components/Chunk";
import Castle from "./entities/Castle";
import Well from "./entities/Well";

export default class CryptideUI {
    debug = false;

    #scene = null;
    #renderer = null;

    #cameraController = null;
    #raycasterController = null;

    #sceneElements = [];
    #chunks = [];

    constructor(canvas, debug = false) {
        if (debug === true){
            this.debug = new GUI();
        }
        this.canvas = canvas;
        this.screenDimensions = {
            width: canvas.width,
            height: canvas.height
        }
        /* Scene */
        this.#buildScene();
        /* Renderer */
        this.#buildRender(this.screenDimensions);
        /* CameraController */
        this.#cameraController = new CameraController(this.screenDimensions, this.#renderer, this.debug);
        /* RaycasterController */
        this.#raycasterController = new RaycasterController(this.#cameraController,this.#scene);

        this.#setup();
        requestAnimationFrame(()=>this.render());
    }
    async #setup(){
        this.clock = new THREE.Clock();
        /* Fetch ressources */
        await Chunk.fetchChunks();
        /* Lights */
        this.#buildLights();
        return this;
    }
    #buildRender() {
        this.#renderer = new THREE.WebGLRenderer({canvas: this.canvas, antialias: true});
        const DPR = (window.devicePixelRatio) ? window.devicePixelRatio : 1;
        this.#renderer.setPixelRatio(DPR);
        this.#renderer.setSize(this.screenDimensions.width, this.screenDimensions.height);

        this.#renderer.gammaInput = true;
        this.#renderer.gammaOutput = true;

        this.#renderer.shadowMap.enabled = true;
        this.#renderer.shadowMap.type = THREE.PCFSoftShadowMap;

        return this.#renderer;
    }

    #buildScene() {
        this.#scene = new THREE.Scene();
        const scene = this.#scene;
        scene.background = new THREE.Color('#273833');
        // scene.fog = new THREE.FogExp2( 0xcccccc, 0.015 );
    }

    #buildLights() {
        const scene = this.#scene;

        const dirLight1 = new THREE.DirectionalLight( 0xffffff, 0.6 );
        dirLight1.position.set( 4, -67, 100 );
        dirLight1.castShadow = true;
        dirLight1.shadow.radius = 2;
        dirLight1.shadow.mapSize.width = this.screenDimensions.width;
        dirLight1.shadow.mapSize.height = this.screenDimensions.height;
        dirLight1.shadow.camera.top = this.screenDimensions.height / 20;
        dirLight1.shadow.camera.right = this.screenDimensions.width / 20;
        dirLight1.shadow.camera.bottom = this.screenDimensions.height / -200;
        dirLight1.shadow.camera.left = this.screenDimensions.width / -200;
        dirLight1.shadow.camera.near = -1000;
        dirLight1.shadow.camera.far = 1000;

        if(this.debug){
            const shadowDebug = this.debug.addFolder('Shadows')
            shadowDebug.add( dirLight1.position, 'x', -100, 100, 0.01 ).name('X-axis');
            shadowDebug.add( dirLight1.position, 'y', -100, 100, 0.01 ).name('Y-axis');
            shadowDebug.add( dirLight1.position, 'z', -100, 100, 0.01 ).name('Z-axis');
            shadowDebug.add( dirLight1, 'castShadow' ).name('Enable shadows');
        }

        let helper = new THREE.CameraHelper( dirLight1.shadow.camera,10 );
        //scene.add( helper );

        scene.add( dirLight1 );


        const ambientLight = new THREE.AmbientLight( 0xffffff, 0.4 );
        scene.add( ambientLight );
    }

    #update() {
        const elapsedTime = this.clock.getElapsedTime();
        for(let i=0; i<this.#sceneElements.length; i++){
            switch(this.#sceneElements[i].type){
                case 'Object3D':
                    for (let y in this.#sceneElements[i].children){
                        let tile = this.#sceneElements[i].children[y];
                        tile.update(elapsedTime);
                    }
                    break;
            }
        }

        if (this.#cameraController.getControls() !== null) this.#cameraController.getControls().update();
        TWEEN.update();
        this.#renderer.render(this.#scene, this.#cameraController.getCamera());
    }

    render() {
        requestAnimationFrame(()=>this.render());
        this.#update()
    }

    /**
     * Load a party from a json file
     * @param party
     */
    loadScene(party) {
        // Chunks
        this.#chunks = [];
        for(let x=0; x<party.chunks.length; x++){
            for(let y=0; y<party.chunks[x].length; y++){
                let chunk = party.chunks[x][y];
                let chunkObject = new Chunk({
                    id: chunk.id,
                    reversed: chunk.reverse,
                    startingPosition: [x, y]
                });
                this.#chunks.push(chunkObject);
                this.#sceneElements.push(chunkObject);
            }
        }
        // Structures
        for(let index in party.structures){
            let struct = party.structures[index]
            let chunkPos = [Math.floor(struct.positions[0] / 3), Math.floor(struct.positions[1] / 6)];
            let tiles = this.#chunks[chunkPos[0]*2 + chunkPos[1]].tiles;
            let chunkLocalPos = [struct.positions[0] % 3, struct.positions[1] % 6];
            let tile = tiles[chunkLocalPos[0]*6 + chunkLocalPos[1]];
            if(struct.type === "castle")
                tile.addEntity(new Castle(struct.color));
            else if(struct.type === "well")
                tile.addEntity(new Well(struct.color));
        }
        // Add to map
        for(let i=0; i<this.#sceneElements.length; i++) {
            this.#scene.add(this.#sceneElements[i]);
        }
    }

    /**
     * Return a Tile object from a position
     * @param x
     * @param y
     * @returns {*}
     */
    getTile({x, y}){
        let chunkPos = [Math.floor(x / 3), Math.floor(y / 6)];
        let tiles = this.#chunks[chunkPos[0]*2 + chunkPos[1]].tiles;
        let chunkLocalPos = [x % 3, y % 6];
        return tiles[chunkLocalPos[0]*6 + chunkLocalPos[1]];
    }

    /**
     * Return a tile that the user selected
     * @returns {Promise<void>}
     */
    async selectTile(){
        if (this.#chunks.length === 0) return;
        return await this.#raycasterController.selectTile();
    }

    /**
     * Clear the scene
     */
    clearScene() {
        this.#sceneElements = [];
        this.#scene.clear();
    }
    onWindowResize() {
        const {width, height} = this.canvas;
        this.screenDimensions.width = width;
        this.screenDimensions.height = height;
        this.#cameraController.getCamera().aspect = width / height;
        this.#cameraController.getCamera().updateProjectionMatrix();
        this.#renderer.setSize(width, height);
    }

    get raycasterController(){
        return this.#raycasterController;
    }
}
