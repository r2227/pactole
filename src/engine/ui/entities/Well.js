'use strict';
import Entity from "./Entity";

/*
 * Well class extending Object3D
 */
export default class Well extends Entity {
    /**
     * Constructor
     * @param color
     */
    constructor(color) {
        super({url : new URL('./../../../models/3d/entities/well.glb', import.meta.url),color : color,scaling : [0.03,0.03,0.03]});
    }
}
