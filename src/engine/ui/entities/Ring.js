import Entity from "./Entity";
import * as THREE from "three";

export default class Ring extends Entity {
    constructor() {
        super({});
    const geometry = new THREE.RingGeometry( 12, 11, 6 );
    const material = new THREE.MeshBasicMaterial( { color: 0x000000, side: THREE.DoubleSide } );
    this._base = new THREE.Mesh( geometry, material );
    this._base.rotateZ(Math.PI/2);
    this._base.scale.set(0.08,0.08,0.08);
    this.setOffset({z:0.01});
    this._base.castShadow = true;
    this._base.receiveShadow = true;
    this._base.visible = false;
    this.add(this._base)
    }

    enableVisibility(){
        this._base.visible = true;
    }

    disableVisibility(){
        this._base.visible = false;
    }
}