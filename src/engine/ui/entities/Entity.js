'use strict';
import * as THREE from 'three';
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";

/*
 * Entity class extending Object3D
 */
export default class Entity extends THREE.Object3D {
    _base;
    _offset;
    /**
     * Constructor
     * @param color
     * @param filename
     * @param scaling
     */
    constructor({url,color,scaling}) {
        super();
        if(url){
            this.modelUrl = url;
            if(color && scaling)
            {
                this.#onCreate(color);
                this.scale.set(scaling[0],scaling[1],scaling[2]);
            }
        }
        this._offset = {x:0,y:0,z:0};
    }


    #onCreate(color) {
        new GLTFLoader().load(
            this.modelUrl.href,gltf  => {
                let obj = this._base = gltf.scene;
                // Setup rotation
                obj.rotateX( Math.PI / 2);

                // Shadow
                gltf.scene.traverse( function( node ) {

                    if ( node.isMesh ) { node.castShadow = true; }

                } );

                // Color based on type
                this.updateColor(color);
                // add the Tile OBJ to the Object3D group
                this.add(obj);
            }
        );
    }

    setOffset({x,y,z}){
        if(x)
        {
            this.translateX(x);
            this._offset.x = x;
        }

        if(y)
        {
            this.translateY(y);
            this._offset.y = y;
        }

        if(z)
        {
            this.translateZ(z);
            this._offset.z = z;
        }

    }

    updateColor(color) {
        this._base.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
                if(["Dark Brown roof", "Red brown", "Brown red light","roof"].includes(child.material.name)  )
                    child.material.color.set(color);
            }
        }.bind(this));
    }

    update(time) {
        // this.rotation.y +=  0.01;

    }

    get material(){
        return this._base.material;
    }

}
