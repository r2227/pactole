'use strict';
import Entity from "./Entity";

/*
 * Castle class extending Object3D
 */
export default class Castle extends Entity {
    /**
     * Constructor
     * @param color
     */
    constructor(color) {
        super({url : new URL('./../../../models/3d/entities/castle.glb', import.meta.url),color: color, scaling: [0.20, 0.20, 0.20]});
    }
}
