'use strict';
import * as THREE from "three";
import {GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import { TWEEN } from "three/examples/jsm/libs/tween.module.min";
import Entity from "./Entity";
import Pawn from "./Pawn";
import Ring from "./Ring";

const forestColors = ['#45A82F'];
const desertColors = ['#FB8E00'];
const lakeColors = ['#44E5EB'];
const swampColors = ['#1f092f'];
const mountainColors = ['#212121'];

const depthLake = 0.3;

export default class Tiles extends Entity{
    #localCoordinates = [0, 0, 0];
    #globalCoordinates = [0, 0, 0];
    #offset;

    #positionOffset = [0, 0, 0]; // Used for animations

    #decorations = [];
    #entities = [];

    #ring;

    #animal = null;
    #ringVisible = false;
    #ringBorder = null;
    #highlightWhite = null;
    #highlightBlack = null;

    static coordinatesToPosition(x, y ,z) {
        let pos = [
            2*x*0.866,
            y*1.5,
            z
        ]
        if (y % 2 === 1){
            pos[0] += 0.866;
        }
        return pos;
    }

    /**
     * Tiles constructor
     * @param {('forest'|'desert'|'lake'|'swamp'|'mountain')} type
     */
    constructor(type = "forest") {
        super({url : new URL('./../../../models/3d/tiles/tile.glb', import.meta.url)});
        if(type !== "lake")
            this.#offset = Math.random()*0.2-0.1
        this.#onCreate(type);
    }

    getPosition(){
        let globalCoordinates = this.#globalCoordinates;
        let positions = Tiles.coordinatesToPosition(globalCoordinates[0], globalCoordinates[1] , globalCoordinates[2]);
        return [positions[0] + this.#positionOffset[0], positions[1] + this.#positionOffset[1], positions[2] + this.#positionOffset[2]];
    }
    getGlobalCoordinates(){
        return this.#globalCoordinates;
    }

    setGlobalCoordinates(x, y, z) {
        let pos3D = Tiles.coordinatesToPosition(x, y ,z);
        this.position.set(pos3D[0], pos3D[1], pos3D[2]);

        this.#globalCoordinates = [x, y, z];
        this.setLocalCoordinates(x%3, y%6, z);
        return this;
    }
    setLocalCoordinates(x, y, z) {
        this.#localCoordinates = [x, y, z];
        return this;
    }

    /**
     * Initialize the tile
     * @param type
     */
    #onCreate(type) {
        new GLTFLoader().load(
            this.modelUrl.href,gltf => {
                let obj = gltf.scene;
                // Setup rotation
                obj.rotateX( Math.PI / 2);

                if(type !== 'lake')
                    obj.translateY(this.#offset);

                // Outline
                obj.traverse( function ( child ) {
                    if ( child instanceof THREE.Mesh ) {
                        if(child.name === "base") {
                            this._base = child;
                            this.updateMaterials(type);
                            child.castShadow = true;
                            child.receiveShadow = true;
                            /*let edges = new THREE.EdgesGeometry(child.geometry, 12);
                            edges.rotateX(Math.PI);
                            let line = new THREE.LineSegments(edges, new THREE.LineBasicMaterial({
                                color: 0x000000
                            }));
                            switch (type) {
                                case 'lake':
                                    line.scale.set(1, 1, 1 - depthLake);
                            }
                            line.name = 'tile_outline';
                            // Add the lines
                            this.add(line);*/
                        }
                        if(child.name === "ring"){
                            this.#ring = child;
                            if(this.#ringVisible)
                                this.setRingVisible()
                            else
                                this.setRingInvisible()
                            if(this.#animal !== null)
                                this.setRingColor(this.#animal);

                        }
                    }
                }.bind(this));

                // add the Tile OBJ to the Object3D group
                this.add(obj);
            }
        );
        this.name = "tile";
        this.#ringBorder = new Ring();
        this.addEntity(this.#ringBorder);
    }

    /**
     * Update the materials of the tile
     * @param type
     */
    updateMaterials(type) {
        this.setBaseColor(type);
    }

    /**
     * Set the base color of the tile
     * @param type
     */
    setBaseColor(type) {
        this._base.traverse(child => {
            let newColor = '#FFFFFF';
            switch (type){
                case 'desert':
                    newColor = desertColors[Math.floor(Math.random()*desertColors.length)];
                    break;
                case 'lake':
                    newColor = lakeColors[Math.floor(Math.random()*lakeColors.length)];
                    // Reduce height
                    child.scale.set(1,1,1-depthLake);
                    // Move & save the offset
                    this.position.z = -depthLake;
                    this.#positionOffset[2] = -depthLake;
                    break;
                case 'swamp':
                    newColor = swampColors[Math.floor(Math.random()*swampColors.length)];
                    break;
                case 'mountain':
                    newColor = mountainColors[Math.floor(Math.random()*mountainColors.length)];
                    break;
                case 'forest':
                    newColor = forestColors[Math.floor(Math.random()*forestColors.length)];
                    break;
                default:
                    newColor = '#FFFFFF';
                    break;
            }
            child.material = new THREE.MeshLambertMaterial({ color: new THREE.Color(newColor) });
            child.material.flatShading = true;
        });
    }
    /**
     * Set the ring color
     * @param type
     */
    setRingColor(type){
        if(this.#ring === undefined)
        {
            this.#animal = type;
            return;
        }
            let newColor = '#FFFFFF';
            switch (type){
                case 'puma':
                    newColor = '#FF0000';
                    break;
                case 'bear':
                    newColor = '#000000';
                    break;
            }
            this.#ring.material.color.set( newColor );
            this.#ring.material.flatShading = true;
    }

    dispose() {
        // Dispose everything that was created in this class - GLTF model, materials etc.
    }

    addEntity(entity) {
        this.#entities.push(entity);
        this.add(entity);
        entity.setOffset({z:this.#offset});
    }

    addPawn(color,type){
        let level = 0;
        for(let index in this.#entities ){
            if(this.#entities[index].name === "pawn")
                level++;
        }
        this.addEntity((new Pawn(type,color,level)));
    }

    bounce = () => {
        let positions = this.getPosition();
        new TWEEN.Tween(this.position)
            .to({z: positions[2]+0.7}, 250)
            .easing(TWEEN.Easing.Cubic.Out)
            .start()
            .onComplete(() => {
                    new TWEEN.Tween(this.position)
                        .to({z: positions[2]}, 250)
                        .easing(TWEEN.Easing.Cubic.In)
                        .start()
                }
            )
        setTimeout(this.bounce, (Math.random()*100%1000) + 500);
    }

    select = () => {
        let positions = this.getPosition();
        new TWEEN.Tween(this.position)
            .to({z: positions[2]+0.5}, 250)
            .easing(TWEEN.Easing.Cubic.Out)
            .start()
    }

    unselect = () => {
        let positions = this.getPosition();
        new TWEEN.Tween(this.position)
            .to({z: positions[2]}, 250)
            .easing(TWEEN.Easing.Cubic.Out)
            .start()
    }

    setRingVisible = () => {
        if(this.#ring !== undefined)
            this.#ring.visible = true;
        this.#ringVisible = true;
    }

    setRingInvisible = () => {
        if(this.#ring !== undefined)
            this.#ring.visible = false;
        this.#ringVisible = false;
    }

    enableHighlight = () => {
        this.#ringBorder.enableVisibility();
        this.#highlightWhite = new TWEEN.Tween(this.#ringBorder.material.color)
                .to({r:1,g:1,b:1},2000)
                .easing(TWEEN.Easing.Cubic.Out);
        this.#highlightBlack = new TWEEN.Tween(this.#ringBorder.material.color)
            .to({r:0,g:0,b:0},2000)
            .easing(TWEEN.Easing.Cubic.Out);

        this.#highlightWhite.chain(this.#highlightBlack);
        this.#highlightBlack.chain(this.#highlightWhite);
        this.#highlightWhite.start();
    }

    disableHighlight = () => {
        this.#ringBorder.disableVisibility();
        this.#highlightWhite.stop();
        this.#highlightBlack.stop();

    }

}
