'use strict';
import * as THREE from 'three';
import Entity from "./Entity";
/*
 * Pawn class
 */
export default class Pawn extends Entity{

    /**
     * Constructor
     * @param type
     * @param color
     * @param level
     */
    constructor(type,color,level) {
        super({});
        this.name = "pawn";
        let geometry;
        const material = new THREE.MeshLambertMaterial( {color: color} );
        if(type === 'no')
        {
            geometry = new THREE.BoxGeometry( 0.2, 0.2, 0.2 );
            geometry.rotateZ(Math.random()*360);
        }

        if(type === 'yes')
        {
            geometry = new THREE.CylinderGeometry( 0.2, 0.2, 0.2,30 );
            geometry.rotateX(Math.PI/2);
        }
        this._base = new THREE.Mesh( geometry, material );
        this._base.translateZ(this._offset.z + 0.1 + level*0.2);
        this._base.translateX(Math.random()*0.06-0.03);
        this._base.translateY(Math.random()*0.06-0.03);
        this._base.castShadow = true;
        this._base.receiveShadow = true;

        let edges = new THREE.EdgesGeometry(this._base.geometry, 15);

        let line = new THREE.LineSegments(edges, new THREE.LineBasicMaterial({
            color: 0x000000
        }));
        this._base.add(line)
        this.add(this._base)
    }
}
