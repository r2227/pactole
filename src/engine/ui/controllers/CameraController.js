import * as THREE from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import { TWEEN } from "three/examples/jsm/libs/tween.module.min";
import Tiles from "../entities/Tiles";

export default class CameraController {
    #camera = null;
    #controls = null;

    #oldZoom = 0;
    #oldPosition = [];
    #oldRotation = [];
    #oldTarget = [];

    constructor({width, height}, renderer, debug) {
        /* CAMERA */
        const nearPlane = -1000;
        const farPlane = 1000;

        /* Orthographic */
        const camera = new THREE.OrthographicCamera(
            width / -2,
            width / 2,
            height / 2,
            height / -2,
            nearPlane,
            farPlane
        );

        // Default settings
        camera.position.set(8,8.33,10);
        camera.up = new THREE.Vector3(0,0,1);
        camera.zoom = 7;
        camera.lookAt(7,8.33,0);

        /* Debug Options */
        if(debug)
        {
            const cameraDebug = debug.addFolder('Camera');
            cameraDebug.add( camera, 'zoom', 0, 100, 0.5 ).name('Zoom');
            cameraDebug.add( camera.rotation, 'x', 0, 2*Math.PI, 0.01 ).name('X-axis');
            cameraDebug.add( camera.rotation, 'y', 0, 2*Math.PI, 0.01 ).name('Y-axis');
            cameraDebug.add( camera.rotation, 'z', 0, 2*Math.PI, 0.01 ).name('Z-axis');
        }

        this.#camera = camera;

        /* CONTROL */
        this.#controls = new OrbitControls( this.#camera, renderer.domElement );
        let controls = this.#controls;
        controls.listenToKeyEvents( window ); // optional

        //controls.addEventListener( 'change', render ); // call this only in static scenes (i.e., if there is no animation loop)
        let tilesPosition = Tiles.coordinatesToPosition(8/2,11/2,0/2);
        controls.target.set( tilesPosition[0], 8.33, tilesPosition[2] );
        controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
        controls.dampingFactor = 0.05;

        controls.screenSpacePanning = true;

        controls.minDistance = 10;
        controls.maxDistance = 25;

        controls.maxPolarAngle = Math.PI / 2.5;
        controls.mouseButtons = { LEFT: null, MIDDLE: THREE.MOUSE.MIDDLE, RIGHT: THREE.MOUSE.LEFT };
        //controls.mouseButtons = { LEFT: null, MIDDLE: null, RIGHT: null };

        this.#oldZoom = this.#camera.zoom;
        this.#oldPosition = [camera.position.x, camera.position.y, camera.position.z];
        this.#oldRotation = [camera.rotation.x, camera.rotation.y, camera.rotation.z];
        this.#oldTarget = [controls.target.x, controls.target.y, controls.target.z];
    }

    viewTile(tile) {
        let position = tile.getPosition();
        let offsetCamera = [position[0], position[1], position[2]];
        offsetCamera[0] += 7;
        offsetCamera[2] = this.#camera.position.z;
        this.#tweenTransition(offsetCamera, position, undefined, 1000);
    }

    revert() {
        this.#camera.position.set(this.#oldPosition[0], this.#oldPosition[1], this.#oldPosition[2]);
        this.#camera.rotation.set(this.#oldRotation[0], this.#oldRotation[1], this.#oldRotation[2]);
        this.#controls.update();
        this.#controls.target.set(this.#oldTarget[0], this.#oldTarget[1], this.#oldTarget[2]);
    }
    smoothRevert(){
        this.#tweenTransition(this.#oldPosition, this.#oldTarget, this.#oldRotation, 1000);
    }

    getCamera(){
        return this.#camera;
    }

    getControls(){
        return this.#controls;
    }

    #tweenTransition(position, target, rotation, duration) {
        // Cancel transition :[
        return;
        // TODO: TWEEN Rotation
        let camera = this.#camera;
        let controls = this.#controls;

        // LookAt
        let controlsTarget = this.#controls.target;
        new TWEEN.Tween(controlsTarget)
            .to({
                x: target[0],
                y: target[1],
                z: target[2]
            }, duration)
            .easing(TWEEN.Easing.Quadratic.InOut)
            .onUpdate(function() {
                // controls.target.set(target.x, target.y, target.z);
            }).start();
        // Camera Position
        new TWEEN.Tween(camera.position)
            .to({
                x: position[0],
                y: position[1],
                z: position[2]
            }, duration)
            .easing(TWEEN.Easing.Quadratic.InOut)
            .onUpdate(function() {
                controls.update();
            }).onComplete(function() {
            // console.log("done");
        }).start();
    }
}
