import * as THREE from "three";

export default class RaycasterController{
    #raycaster = new THREE.Raycaster();
    #pointer = new THREE.Vector2();
    #enableRaycast = false;
    #cameraController = null;
    #scene = null;

    // Object to be raycasted
    #intersectObject;
    #selectedObject;

    #clickHandler = null;
    #selectable = [];

    constructor(cameraController, scene) {
        this.#raycaster.params.Line.threshold = 0.05;
        this.#cameraController = cameraController;
        this.#scene = scene;
        window.addEventListener( 'pointermove', ()=>this.#onTileHover(event), false );
        window.addEventListener('click', ()=>this.#onTileClick(event), false);
    }

    #onPointerMove( event ) {
        this.#pointer.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        this.#pointer.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
    }

    #raycast(){
        if(!this.#enableRaycast || !this.#pointer || !this.#selectable)
            return null;
        this.#raycaster.setFromCamera( this.#pointer, this.#cameraController.getCamera() );
        return this.#raycaster.intersectObjects( this.#selectable );
    }

    #onTileHover(event){
        let intersects = this.#getIntersects(event);
        if (intersects && intersects.length > 0) {
            let object = intersects[0].object;
            let tile;

            while(object.parent != null && object.name !== "tile")
            {
                object = object.parent;
            }

            if(object.name === "tile")
                tile = object;

            if(tile && this.#intersectObject !== tile)
            {
                if((this.#intersectObject && this.#selectedObject && this.#intersectObject !== this.#selectedObject) || (this.#intersectObject && !this.#selectedObject))
                    this.#intersectObject.unselect();
                this.#intersectObject = tile;
                tile.select();
            }


        } else {
            if((this.#intersectObject && this.#selectedObject && this.#intersectObject !== this.#selectedObject) || (this.#intersectObject && !this.#selectedObject))
            {
                this.#intersectObject.unselect();
                this.#intersectObject = null;
            }
        }
    }

    /**
     * Click event handler
     * @param event
     */
    #onTileClick(event){
        let intersects = this.#getIntersects(event);
        if(intersects && intersects.length > 0)
        {
            let object = intersects[0].object;
            let tile;

            while(object.parent != null && object.name !== "tile")
            {
                object = object.parent;
            }
            if(object.name === "tile")
                tile = object;
            if(tile)
            {
                if(this.#selectedObject) {
                    this.#selectedObject.unselect();
                }
                if(this.#selectedObject && tile === this.#selectedObject){
                    this.#selectedObject = null;
                    this.#cameraController.smoothRevert();
                    if (typeof this.#clickHandler === "function")
                        this.#clickHandler(tile);
                }else{
                    this.#selectedObject = tile;
                    this.#selectedObject.select();
                    this.#cameraController.viewTile(tile);
                }
            }

        }
    }

    /**
     * Get the objects to intersects
     * @param event
     * @returns {*[]}
     */
    #getIntersects( event )
    {
        event.preventDefault()
        this.#onPointerMove(event)
        return this.#raycast();
    }

    disableRaycast(){
        this.#enableRaycast = false;
    }

    enableRaycast(){
        this.#enableRaycast = true;
    }

    async selectTile(){
        if(!this.#enableRaycast)
            this.#enableRaycast = true;
        // Let the user move and click
        let tile = await new Promise(resolve => {
            // Bindings
            this.#clickHandler = resolve;
        });
        this.#clickHandler = null;
        this.#enableRaycast = false;
        return tile;
    }

    addSelectable(selectable){
        this.#selectable.push(selectable);
        selectable.enableHighlight();
    }

    clearSelectable(){
        this.#selectable.forEach(function(selectable){
            selectable.disableHighlight();
        });
        this.#selectable = [];
    }
}
