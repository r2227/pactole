'use strict';

import * as THREE from "three";

import Tiles from "../entities/Tiles";

/*
 * Chunk
 */
export default class Chunk extends THREE.Object3D {

    chunkID = 0;
    reversed = false;

    startingPosition = [0, 0];
    tiles = [];

    static chunks = [];

    constructor({ id, reversed, startingPosition }) {
        super();
        if (typeof(reversed) !== "boolean") {
            this.reversed = false;
        }
        this.chunkID = id;
        this.reversed = reversed;
        this.startingPosition = startingPosition;

        if (Chunk.chunks[this.chunkID-1] !== undefined) {
            let d = Chunk.chunks[this.chunkID-1];

            for (let x = 0; x < 3; x++) {
                for (let y = 0; y < 6; y++) {
                    let hasAnimals = null;
                    let biomes = d['tiles'][x][y].type;
                    let newPosition = [x, y];
                    for(let i in d.animals)
                    {
                        let animals = d.animals[i];
                        if(animals.positions[0] === newPosition[0] && animals.positions[1] === newPosition[1])
                        {
                            hasAnimals = animals;
                        }
                    }
                    if (this.reversed) {
                        newPosition = [2-x, 5-y];
                    }
                    let tile = new Tiles(biomes).setGlobalCoordinates((startingPosition[0] * 3) + newPosition[0],(startingPosition[1] * 6)+ newPosition[1], 0);
                    this.tiles.push(tile);
                    this.add(tile);
                    if(hasAnimals !== null)
                    {
                        tile.setRingColor(hasAnimals.type);
                        tile.setRingVisible();

                    }
                }
            }
        }
    }
    static async fetchChunks(){
        return new Promise(async (resolve, reject) => {
            if (Chunk.chunks.length === 0) {
                let d = await fetch(new URL('./../../../engine/data/presets/chunks.json', import.meta.url).href);
                d.json().then(data => {
                    Chunk.chunks = data;
                    resolve(data);
                }).catch(e => {
                    reject(e);
                });
            }else{
                reject("Chunks already loaded");
            }
        });
    }
    get tiles(){
        return this.tiles;
    }

    update(time) {
        this.children.forEach(child => {
            child.update(time);
        });
    }
}
