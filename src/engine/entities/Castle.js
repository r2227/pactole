export default class Castle{
    #color;
    #position;

    /**
     * Build a new castle based on the given color and position.
     * @param color
     * @param position
     */
    constructor(color, position) {
        this.#color = color;
        this.#position = position;
    }
}