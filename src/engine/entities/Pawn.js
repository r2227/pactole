export default class Pawn{
    #type;
    #color;
    #owner;

    /**
     * Build a new pawn based on the given type and color
     * @param type - can be a yes or a no
     * @param color - references to a player color
     */
    constructor(type,color,owner) {
        this.#type = type;
        this.#color = color;
        this.#owner = owner;
    }

    get type() {
        return this.#type;
    }

    get color() {
        return this.#color;
    }

    get owner() {
        return this.#owner;
    }
}