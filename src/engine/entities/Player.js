export default class Player{
    static playersColors = {
        'green': 0x00FF00,
        'blue': 0x0000FF,
        'yellow': 0xFFFF00,
        'purple': 0xFF00FF,
        'orange': 0xFF7F00
    };

    #id;
    #username;
    #color;

    /**
     * Build a new player based on the given color and name
     * @param color
     * @param name
     */
    constructor({id, username, color}) {
        this.#id = id;
        this.#username = username;
        this.#color = color;
    }

    get id(){
        return this.#id;
    }
    set color(color){
        this.#color = color;
    }
    get color(){
        return this.#color;
    }

    set username(username){
        this.#username = username;
    }
    get username(){
        return this.#username;
    }

    /**
     * Update the player information
     * @param data
     */
    update(data){
        this.#id = data.id;
        this.#username = data.username;
        this.#color = data.color;
    }

    /**
     * Return the player information as an object
     * @returns {{color, id, username}}
     */
    getData(){
        return {
            id: this.#id,
            username: this.#username,
            color: this.#color
        };
    }

    /**
     * Return a random color name
     * @returns {string}
     */
    static getRandomColorName() {
        let keys = Object.keys(Player.playersColors);
        return keys[Math.floor(Math.random() * keys.length)];
    }
}