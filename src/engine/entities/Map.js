import Castle from "./Castle";
import Well from "./Well";
import Pawn from "./Pawn";

export default class Map {
    // CryptideUI
    #ui;
    // Ressources
    #chunksDB = [];

    // Properties
    #tiles = [];
    #structures = [];

    /**
     * Instantiate a new map and load it's chunks
     * @param {CryptideUI} ui
     */
    constructor(ui) {
        this.#ui = ui;
        fetch(new URL('./../data/presets/chunks.json', import.meta.url))
            .then(response => response.json())
            .then(data => {
                this.#chunksDB = data;
            });
    }

    /**
     * Load the tiles and structures of the map
     * @param {Object} map
     */
    loadMap(map) {
        // Chunks
        this.#tiles = [];
        for (let i = 0; i < map.chunks.length; i++) {
            for (let y = 0; y < map.chunks[i].length; y++) {
                let chunk = this.#loadChunk(map.chunks[i][y]);
                let animals = chunk.animals;
                let tiles = chunk.tiles;
                for (let x = 0; x < 3; x++) {
                    for (let z = 0; z < 6; z++) {
                        if(this.#tiles[(i*3)+x] === undefined) this.#tiles[(i*3)+x] = [];
                        let tile = this.#tiles[(i*3)+x][(y*6)+z] = tiles[x][z];
                        tile['position'] = {x: (i*3)+x, y: (y*6)+z};
                        tile['pawn'] = [];
                        tile.hasNo = () => {
                            for (let i = 0; i < tile.pawn.length; i++) {
                                if(tile.pawn[i].type === 'no') return true;
                            }
                            return false;
                        };
                        let animal = animals.find(elem => (elem.positions[0] === x && elem.positions[1] === z));
                        if(animal !== undefined) {
                            tile.animal = animal;
                        }
                    }
                }
            }
        }

        // Structures
        for(let index in map.structures){
            let struct = map.structures[index];
            this.#tiles[struct.positions[0]][struct.positions[1]].structure = struct;
            if(struct.type === "castle")
            {
                struct = new Castle(struct.color,struct.positions);
            }
            else if(struct.type === "well")
                struct = new Well(struct.color,struct.positions);
            this.#structures.push(struct);
        }
    }

    /**
     * Get the tiles of a chunk
     * @param {int} id
     * @param {boolean} reversed
     */
    #loadChunk({id, reversed}) {
        const chunk = this.#chunksDB.find(chunk => chunk.id === id);
        return chunk;
    }

    /**
     * Get the tiles of the map
     * @returns {*[]}
     */
    get tiles() {
        return this.#tiles;
    }

    /**
     * Add a pawn to a tile
     * @param x
     * @param y
     * @param color
     * @param type
     * @param owner
     */
    addPawn(x, y, color, type, owner) {
        this.#tiles[x][y].pawn.push(new Pawn(type, color, owner));
    }

    /**
     * Get the pawns of a tile
     * @param x
     * @param y
     * @returns {[]}
     */
    getTilePawns(x, y) {
        return this.#tiles[x][y].pawn;
    }

    get clickeableTiles() {
        /*
        let tileChoice = [];
        let mapTiles = this.#tiles;
        for (let woops in mapTiles){
            mapTiles[woops] = mapTiles[woops].map(maCase => {
                if (!maCase.structure){
                    return { x: maCase.position.x, y: maCase.position.y}
                }
            });
            tileChoice = tileChoice.concat(mapTiles[woops]);
        }
        return tileChoice.filter(elem => elem !== undefined);
        */
        // TODO: Better shit cuz thats shit
        let lTiles = [];
        for (let i = 0; i < this.#tiles.length; i++) {
            for (let y = 0; y < this.#tiles[i].length; y++) {
                if (!this.#tiles[i][y].structure && !this.#tiles[i][y].hasNo()) {
                    lTiles.push({x: i, y: y});
                }
            }
        }
        return lTiles;
    }

    /**
     * Get the structures of the map
     * @returns {*[]}
     */
    get structures() {
        return this.#structures;
    }

    /**
     * Get tiles around a position (x,y)
     * @param {int} x
     * @param {int} y
     * @param {int} distance
     * @returns {Array}
     */
    getSurroundingTiles(x,y,distance) {
        let tiles = [];
        let addedPositions = [];
        let positions = this.#getSurroundingCoordinates(x,y,distance);
        for (let index in positions){
            if (this.#tiles[positions[index].x] !== undefined && this.#tiles[positions[index].x][positions[index].y] !== undefined && addedPositions.indexOf(positions[index].x+"_"+positions[index].y) === -1) {
                tiles.push(this.#tiles[positions[index].x][positions[index].y]);
                addedPositions.push(positions[index].x + '_' + positions[index].y);
            }
        }
        return tiles;
    }

    /**
     * Get the coordinates of the tiles around a position (x,y)
     * @param x
     * @param y
     * @param distance
     * @returns {{x, y}|*[]|T[]}
     */
    #getSurroundingCoordinates(x, y, distance){
        if (distance === 0) return [{x: x, y: y}];
        let positions = [];
        let neighborsTiles = [
            {x: x-1, y: y-0},
            {x: x-0, y: y-1},
            {x: x+1, y: y-0},
            {x: x-0, y: y+1}
        ];
        positions.push({x: x, y: y}); // Self
        if (y % 2 === 0){
            neighborsTiles = neighborsTiles.concat([
                {x: x-1, y: y-1},
                {x: x-1, y: y+1}
            ]);
        }else{
            neighborsTiles = neighborsTiles.concat([
                {x: x+1, y: y-1},
                {x: x+1, y: y+1}
            ]);
        }
        positions = positions.concat(neighborsTiles);
        for(let index in neighborsTiles){
            let neighbor = neighborsTiles[index];
            positions = positions.concat(this.#getSurroundingCoordinates(neighbor.x, neighbor.y, distance-1));
        }
        return positions;
    }

}