export default class Well{
    #color;
    #position;

    /**
     * Build a new well based on the given color and position.
     * @param color
     * @param position
     */
    constructor(color, position) {
        this.#color = color;
        this.#position = position;
    }
}