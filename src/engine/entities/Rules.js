/*
 * Different type of rules:
 *
 *  Will check if the tile is one of the specified biomes.
 *  - type: 'insideBiomes'
 *      - biomes: ['forest', 'lake']
 *
 * Will check if the tile is one or near 1 tile of the specified biomes or an animal territory.
 * - type: 'nearZone'
 *      - biomes: ['forest']
 *      - animals: boolean
 *
 *  Will check if the tile is within 2 tiles of the specific structures or animals.
 * - type: 'nearElement'
 *      - structures: ['castle']
 *      - animals: ['bear']
 *
 * Will check if the tile is within 3 tiles of a structure with the specified color.
 * - type: 'nearStructure'
 *      - color: 'green'
 *
 * These rules can be reversed by using the 'reverse' parameter.
 *
 *
 */
export default class Rules {
    #type = null;
    #reverse = false;
    #context = null;
    /**
     * Build a new rule based on the given type and reverse flag.
     * @param {String} type
     * @param {boolean} reverse
     * @param {Array} context
     */
    constructor({type, reverse, ...context}) {
        this.#type = type;
        this.#reverse = reverse;
        this.#context = context;
    }

    get type() {
        return this.#type;
    }
    get reverse() {
        return this.#reverse;
    }
    get context() {
        return this.#context;
    }

    /**
     * Check if a move is valid.
     * @returns {boolean}
     * @param {Map} map
     * @param {{x: int,y: int}} move
     * @returns {boolean}
     */
    isValidMove(map, move) {
        let result = false;
        let tiles;
        switch(this.#type){
            case 'insideBiomes':
                tiles = map.getSurroundingTiles(move.x, move.y, 0)[0];
                let allowedBiomes = this.#context.biomes;
                if (allowedBiomes.includes(tiles.type)) {
                    result = true;
                }
                break;
            case 'nearZone':
                tiles = map.getSurroundingTiles(move.x, move.y, 1);
                for(let tile of tiles) {
                    if (this.#context.biomes.includes(tile.type) || (this.#context.animals === true && tile.animal !== undefined)) {
                        result = true;
                        break;
                    }
                }
                break;
            case 'nearElement':
                tiles = map.getSurroundingTiles(move.x, move.y, 2);
                for(let tile of tiles) {
                    if ((tile.structure !== undefined && this.#context.structures.includes(tile.structure.type)) || (tile.animal !== undefined && this.#context.animals.includes(tile.animal.type))) {
                        result = true;
                        break;
                    }
                }
                break;
            case 'nearStructure':
                tiles = map.getSurroundingTiles(move.x, move.y, 3);
                for (let tile of tiles) {
                    if (tile.structure !== undefined && tile.structure.color === this.#context.color) {
                        result = true;
                    }
                }
                break;
        }
        if (this.#reverse) {
            result = !result;
        }
        return result;
    }

    /**
     * Returns all the valid moves for a given map.
     * @param {Map} map
     * @returns {*[]}
     */
    getAvailableMoves(map) {
        let validPositions = [];
        let tiles = map.tiles;
        // Traverse the map and return all valid moves
        for (let i = 0; i < tiles.length; i++) {
            for (let j = 0; j < tiles[i].length; j++) {
                if (this.isValidMove(map, {x: i, y: j}) && map.getSurroundingTiles(i,j,0)[0].structure === undefined && !map.getSurroundingTiles(i,j,0)[0].hasNo()){
                    validPositions.push({x: i, y: j});
                }
            }
        }
        return validPositions;
    }

    /**
     * Returns all the non-valid moves for a given map.
     * @param {Map} map
     * @returns {*[]}
     */
    getUnavailableMoves(map) {
        let validPositions = [];
        let tiles = map.tiles;
        // Traverse the map and return all non-valid moves
        for (let i = 0; i < tiles.length; i++) {
            for (let j = 0; j < tiles[i].length; j++) {
                if (!this.isValidMove(map, {x: i, y: j}) && map.getSurroundingTiles(i,j,0)[0].structure === undefined && !map.getSurroundingTiles(i,j,0)[0].hasNo()) {
                    validPositions.push({x: i, y: j});
                }
            }
        }
        return validPositions;
    }

    toString(){
        let result ='';
        switch (this.#reverse){
            case false:
                result = result + 'Se trouve ';
                break;
            case true:
                result = result + 'Ne se trouve pas ';
                break;
        }
        switch (this.#type){
            case 'insideBiomes':
                    result = result + 'dans ' + this.translateContext(this.#context.biomes[0]) + ' ou ' + this.translateContext(this.#context.biomes[1]) + '.'
                break;
            case 'nearZone':
                result = result + 'dans, ou à 1 case ';
                if (this.#context.animals){
                    result = result = result + ' du Territoire d\'un animal.';
                }
                else{
                    result = result + 'd\'' + this.translateContext(this.#context.structures[0]) + '.';
                }
                break;
            case 'nearElement':
                result = result + 'dans, ou jusqu\'à 2 cases ';
                if(this.#context.animals.length !== 0){
                    result = result + this.translateContext(this.#context.animals[0]) + '.';
                }
                else{
                    result = result + this.translateContext(this.#context.structures[0]) + '.';
                }
                break;
            case 'nearStructure':
                result = result + 'dans, ou jusqu\'à 3 cases ' + this.translateContext(this.#context.color) + '.';
                break;
        }
        return result;
    }
    translateContext(arg){
        switch (arg){
            case 'forest':
                return 'une Forêt';
            case 'desert':
                return 'un Désert';
            case 'mountain':
                return 'une Montagne';
            case 'lake':
                return 'un Lac';
            case 'swamp':
                return 'un Marécage';
            case 'castle':
                return  'd\' un Chateau';
            case 'well':
                return 'd\'un puit';
            case 'puma':
                return 'du Territoire d\'un puma';
            case 'bear':
                return 'du Territoire d\'un ours';
            case 'green':
                return 'd\'une Structure verte';
            case 'blue':
                return 'd\'une Structure bleue';
            case 'white':
                return 'd\'une Structure blanche';
            case 'black':
                return 'd\'une Structure noire';
        }
    }
}