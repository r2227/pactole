import { createApp } from 'vue'
import App from './App.vue'
import VueConfetti from 'vue-confetti'
import VueSweetAlert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

import Cryptide from './engine/game'
import startingModal from './components/startingModal.vue'
import lobbyModal from './components/lobby/lobbyModal.vue'
import gameContent from './components/gameContent.vue';

const debug = false;

//Create app & instance
const application = createApp(App).mount('#app')


const canvas = document.getElementById("game");

// Create the game instance
const game = new Cryptide({canvas: canvas, debug: debug});
let username = '';

function init(){
    // Checking partyId in URL
    let partyId = '';
    if(window.location.hash) {
        partyId = window.location.hash.substring(1);
    }

    const startingModalVue = instanceComponent(startingModal, {'partyId': partyId});
    startingModalVue.element.onChoice(function(choice,pseudo){
        startingModalVue.destroy();
        username = pseudo;
        switch (choice){
            case 'createGame':
                game.createParty({username: username}).then(r => {
                    const lobbyModalVue = instanceComponent(lobbyModal,{'type': 'master', 'id': r.myId, 'username': username, 'partyId': r.partyId, 'partyHost': window.location.protocol + "//" + window.location.host, 'additionalPlayers': r.players}, lobbyListener);
                    const gameVue = instanceComponent(gameContent, {'id': r.myId, 'username': username, 'additionalPlayers': r.players}, gameListener);
                    gameVue.component.use(VueConfetti);
                    gameVue.component.use(VueSweetAlert2);
                    lobbyModalVue.element.onChoice(function(choice){
                        switch (choice){
                            case 'launchGame':
                                if(confirm("Lancer la partie ? ") === true){
                                    lobbyModalVue.destroy();
                                    game.startParty();
                                    startMap();
                                }
                                break;
                            case 'goBack':
                                lobbyModalVue.destroy();
                                init();
                                break;
                        }
                    });
                });
                break;
            case 'joinGame':
                // Load a party (Test)
                game.joinParty({partyId: partyId, username: username}).then(r => {
                    const lobbyModalVue = instanceComponent(lobbyModal,{'id': r.myId, 'username': username, 'partyId': r.partyId, 'partyHost': window.location.protocol + "//" + window.location.host, 'additionalPlayers': r.players}, lobbyListener);
                    const gameVue = instanceComponent(gameContent, {'id': r.myId, 'username': username, 'additionalPlayers': r.players}, gameListener);
                    gameVue.component.use(VueConfetti);
                    gameVue.component.use(VueSweetAlert2);
                }).catch(e => {
                    switch(e){
                        case 'full':
                            alert('main: Party full');
                            break;
                        case 'running':
                            alert('main: Party is already running!');
                            break;
                        case 'could_not_connect':
                            alert('main: Could not connect');
                            break;
                    }
                    init();
                });
                break;
        }
    });
}

function startMap(){
    (async () => {
        let partyConfig = await fetch(new URL('./engine/data/party/1.json', import.meta.url)).then(res => res.json());
        await game.loadParty(partyConfig);
    })();
}

// Initialize the game world
canvasUpdate();
init();

// In the event of any modification done to the canvas, update the game
function canvasUpdate(){
    game.resizeCanvas();
    window.onresize = canvasUpdate
}

function lobbyListener(ComponentInstance){
    function start(){
        game.on('data', handler)
    }
    function destroy(){
        game.removeListener('data', handler);
    }
    function handler(d){
        if (debug)
            console.log('[Cryptide/Vue/LobbyListeners] Received data from server:', d);
        switch(d.type){
            case 'newPlayer':
                ComponentInstance.element.newPlayer(d.data.id, d.data.username);
                break;
            case 'playerQuit':
                ComponentInstance.element.deletePlayer(d.data.id);
                break;
            case 'startGame':
                ComponentInstance.destroy();
                startMap();
                break;
        }
    }
    return {start, destroy};
}

function gameListener(ComponentInstance){
    function start(){
        game.on('data', handler);
        game.on('dataTurn', gameHandler);
        ComponentInstance.element.onMessage((text) => {
            game.sendMessage(text);
        });
        ComponentInstance.element.onChoice((obj) => {
            // Send to game
            console.log('[Cryptide/Vue/GameListeners] Received choice from Vue, sending to game...');
            game.getAction('selectVue', obj);
        });
        ComponentInstance.element.onPrompt((obj) => {
            // Send to game
            console.log('[Cryptide/Vue/GameListeners] Received onPrompt interaction from Vue, sending to game...');
            game.getAction(obj.type, obj);
        });
    }
    function destroy(){
        game.removeListener('data', handler);
        game.removeListener('dataTurn', gameHandler);
    }
    function handler(d){
        if (debug)
            console.log('[Cryptide/Vue/gameListener] Received data from server:', d);
        switch(d.type){
            case 'rule':
                ComponentInstance.element.setRule(d.rule);
                break;
            case 'newPlayer':
                ComponentInstance.element.newPlayer(d.data.id, d.data.username, d.data.color);
                break;
            case 'playerQuit':
                ComponentInstance.element.deletePlayer(d.data.id);
                break;
            case 'updatePlayer':
                for (let i = 0; i < d.data.length; i++){
                    ComponentInstance.element.updatePlayer(d.data[i].id, d.data[i].username, d.data[i].color);
                }
                break;
            case 'message':
                ComponentInstance.element.addMessageChat({id: d.origin, text: d.data.message});
                break;
        }
    }
    function gameHandler(dh){
        if (debug)
            console.log('[Cryptide/Vue/gameListener] Received dataTurn from server:', dh);
        switch(dh.type){
            case 'newTurn':
                ComponentInstance.element.updatePlayerStatus({idCurrent : dh.currentPlayer});
                break;
            case 'roundCryptid':
            case 'roundPlayer':
                ComponentInstance.element.promptInteraction(dh.type, {x: dh.x, y: dh.y, answer: dh.answer});
                break;
            case 'winner':
                ComponentInstance.element.toggleConfetti(dh.name);
                break;
            case 'alert':
                ComponentInstance.element.alert(dh.message);
                break;
        }
    }
    return {start, destroy};
}

/**
 * Instanciate a component
 * @param name
 * @param {Object} props The props to pass to the component
 * @param events
 * @returns {{container: HTMLDivElement, component: App<Element>, destroy: Function, eventHandler, element: Object}} The component instance and the component's Vue instance
 */
function instanceComponent(name,props = {}, events = null) {
    let eventHandler;
    const struct = document.getElementById('struct')
    const container = document.createElement('div');
    struct.appendChild(container);
    let component = createApp(name, props);
    let element = component.mount(container);
    // Add events
    let response = {
        container: container,
        component: component,
        element: element,
        eventHandler: eventHandler,
        destroy: () => {
            if (eventHandler) {
                eventHandler.destroy();
            }
            component.unmount();
            container.remove();
        }
    };
    if (events) {
        eventHandler = events(response);
        eventHandler.start();
    }
    return response;
}