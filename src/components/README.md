Structure de "components"
===========================

> Liste des dossiers et fichiers du dossier 'components'
### Contenu
    .
    ├── chat                      # Dossier component chat
    │   ├── chat.vue                     # Affiche le chat du lobby
    │   ├── gameChat.vue                 # Affiche le chat de la game
    │   └── message.vue                  # Affiche un message dans le chat
    ├── lobby                     # Dossier component lobby
    │   └── lobbyModal.vue               # Affiche le lobby
    ├── firstInteractionModal.vue # Affiche les choix d'interaction lors d'un tour
    ├── gameContent.vue           # Affiche la structure d'habillage autour du canvas
    ├── Player.vue                # Affiche la liste des joueurs et leur couleur
    └── startingModal.vue         # Affiche le premier menu du jeu (page accueil) 

>VueJS 3 : composants ajoutés à App.vue
