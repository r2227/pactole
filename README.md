# Pactole

Jouez au jeu  <a  href="https://roots.games">ici</a>

## Description

Jeu en ligne, peer to peer, inspiré par le célèbre jeu
de société Cryptide.

### Technologies

![npm](https://img.shields.io/npm/v/vue?color=41B883&label=VueJS&style=for-the-badge)
![npm](https://img.shields.io/npm/v/vite?color=orange&label=Vite&style=for-the-badge)

![npm](https://img.shields.io/npm/v/peerjs?color=red&label=PeerJS&style=for-the-badge)
![npm](https://img.shields.io/npm/v/three?color=blue&label=ThreeJS&style=for-the-badge)

## Installation

````shell
git clone https://gitlab.com/r2227/pactole
cd pactole
npm i
````
### Développement

````shell
npm run dev
````
A utiliser pour tester en local.

* Lancement d'un serveur web local (port 3000)
* Vite hot reload

### Déploiement

````shell
npm run build
````
A utiliser pour un hébergement en ligne.

Récuperer le dossier "dist" suite au build.
 
### Screenshot

<img src="https://i.imgur.com/N6on6iW.png">
<i><center>Interaction choix du joueur</center></i>
<img src="https://i.imgur.com/TCzxrFD.png">
<i><center>Interaction avec la carte</center></i>
<img src="https://i.imgur.com/EA9DYpy.png">
<i><center>Victoire d'un joueur</center></i>

## Team

<a href="https://gitlab.com/rphang"><img src="https://i.imgur.com/DA9XrJC.png" width="75"></a>
<a href="https://gitlab.com/tomkowalpicchi"><img src="https://i.imgur.com/Vmj5Kwb.png" width="75"></a>
<a href="https://gitlab.com/romane.ldru"><img src="https://i.imgur.com/PodLi3I.png" width="75"></a>


